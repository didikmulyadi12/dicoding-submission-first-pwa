const data = [
  {
    id: 1,
    name: "Didik Mulyadi",
    email: "didikmulyadi12@gmail.com",
    profile:
      "https://d17ivq9b7rppb3.cloudfront.net/small/avatar/20200821152311a7e3e4ccf5c506dc763601471bbbd7f0.png",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/didikmulyadi25"
      },
      {
        id: 2,
        type: "facebook",
        username: "https://web.facebook.com/didikmulyadi.25"
      }
    ]
  },
  {
    id: 2,
    name: "Wahyu Romadhon",
    email: "wahyuromadhon@gmail.com",
    profile: "https://sumeks.co/assets/foto/2019/05/ariel.jpg",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/wahuy"
      },
      {
        id: 2,
        type: "linkedln",
        username: "https://id.linkedin.com/in/wahyu-romadhon-39ba27193"
      }
    ]
  },
  {
    id: 3,
    name: "Farhan Kurniawan",
    email: "farhankurniawan@gmail.com",
    profile:
      "https://static.republika.co.id/uploads/images/inpicture_slide/justin-bieber-_190506080333-629.jpg",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/faroutakhtar/?hl=id"
      },
      {
        id: 2,
        type: "linkedln",
        username: "https://id.linkedin.com/in/farhan-rinarusdi"
      }
    ]
  },
  {
    id: 4,
    name: "Aji Saputra",
    email: "ajisaputra@gmail.com",
    profile:
      "https://cdn1-production-images-kly.akamaized.net/qkkE9FlT9arId2Mq2mL7tIz9voA=/640x480/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3188963/original/004515400_1595561934-Anji_3.jpg",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/ajisaputra014/"
      },
      {
        id: 2,
        type: "linkedln",
        username: "https://id.linkedin.com/in/aji-saputra-350416117"
      }
    ]
  },
  {
    id: 5,
    name: "Raharjo Budianto",
    email: "raharjobudianto@gmail.com",
    profile:
      "https://d17ivq9b7rppb3.cloudfront.net/small/avatar/20200821152311a7e3e4ccf5c506dc763601471bbbd7f0.png",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/raharjo/?hl=id"
      },
      {
        id: 2,
        type: "linkedln",
        username: "https://id.linkedin.com/in/raharjo-nirmolo-081182136"
      }
    ]
  },
  {
    id: 6,
    name: "Yosep Paula",
    email: "yoseppaula@gmail.com",
    profile:
      "https://cdn.idntimes.com/content-images/community/2020/08/naruto-seventh-hokage-wallpaper23-2251a98e7b1c9412075c372515d65633_600x400.jpg",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/yoseph_tn/"
      },
      {
        id: 2,
        type: "linkedln",
        username: "https://id.linkedin.com/in/yosep-teki-nugroho-378817102"
      }
    ]
  },
  {
    id: 7,
    name: "Gladys Dys",
    email: "gladys@gmail.com",
    profile:
      "https://i.pinimg.com/originals/2c/4d/30/2c4d3071718ca1ebddd150be2d60cbc1.jpg",
    socialmedia: [
      {
        id: 1,
        type: "instagram",
        username: "https://www.instagram.com/didikmulyadi25"
      },
      {
        id: 2,
        type: "linkedln",
        username: "https://id.linkedin.com/in/gladysgaj-000505118"
      }
    ]
  }
];

const iconUrl = {
  instagram: "/icons/instagram-icon.png",
  facebook: "/icons/facebook-icon.png",
  linkedln: "/icons/linkedln-icon.png"
};

const bookmarkKey = "BOOKMARK";
