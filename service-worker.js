const CACHE_NAME = "firstpwa-v4";
const urlsToCache = [
  "/",
  "/manifest.json",
  "/images/contact-animation.gif",
  "/icons/icon.png",
  "/icons/instagram-icon.png",
  "/icons/facebook-icon.png",
  "/icons/linkedln-icon.png",
  "/nav.html",
  "/index.html",
  "/pages/home.html",
  "/pages/about.html",
  "/pages/contacts.html",
  "/pages/bookmarks.html",
  "/css/main.css",
  "/css/materialize.min.css",
  "/js/materialize.min.js",
  "/js/data.js",
  "/js/main.js",
];

self.addEventListener("install", function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener("activate", function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (cacheName != CACHE_NAME) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener("fetch", function(event) {
  event.respondWith(
    caches
      .match(event.request, { cacheName: CACHE_NAME })
      .then(function(response) {
        return response || fetch(event.request);
      })
  );
});
