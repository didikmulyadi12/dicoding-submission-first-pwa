const getPage = () => {
  const page = window.location.hash.substr(1);
  if (page === "") {
    return "home";
  }

  return page;
};

const isSupportStorage = () => typeof Storage !== "undefined";

const isBookmarked = id => {
  return JSON.parse(localStorage.getItem(bookmarkKey) || '[]').includes(id);
};

const saveToLocalStorage = id => {
  localStorage.setItem(
    bookmarkKey,
    JSON.stringify([
      id,
      ...(JSON.parse(localStorage.getItem(bookmarkKey) || '[]'))
    ])
  );

  renderContentContactPage();
};

const removeFromLocalStorage = id => {
  localStorage.setItem(
    bookmarkKey,
    JSON.stringify(
      JSON.parse(localStorage.getItem(bookmarkKey)).filter(val => val !== id)
    )
  );

  renderBookmarkPage();
};

const getContactCardElement = value => {
  let socialList = ``;

  value.socialmedia.forEach(social => {
    socialList += `
      <li class="social-item">
        <a href="${social.username}" target="_blank">
          <img src="${iconUrl[social.type]}" alt="${social.type}"/>
        </a>
      </li>
    `;
  });

  return `
    <div class="contact-info col s12 m6 l6">
      <div class="card">
        <div class="contact-image" style="background-image: url('${
          value.profile
        }');"></div>
        <div class="card-content">
          <span class="card-title">${value.name}</span>
          <div class="card-subtitle">${value.email}</div>
          <p>Social Media</p>
          <ul>${socialList}</ul>
          ${
            isSupportStorage() &&
            getPage() === "contacts" &&
            !isBookmarked(value.id)
              ? `
            <button class="bookmark" data-id="${value.id}" onclick="saveToLocalStorage(${value.id})">
              <i class="small material-icons">bookmark_border</i> Mark
            </button>
          `
              : ""
          }

          ${
            isSupportStorage() &&
            getPage() === "bookmarks" &&
            isBookmarked(value.id)
              ? `
            <button class="unbookmark" data-id="${value.id}" onclick="removeFromLocalStorage(${value.id})">
              <i class="small material-icons">delete_outline</i> Mark
            </button>
          `
              : ""
          }
        </div>
      </div>
    </div>
  `;
};

const renderContentContactPage = () => {
  const emailsElement = document.querySelector("#contacts-content");
  let emailList = ``;

  data.forEach(value => (emailList += getContactCardElement(value)));
  emailsElement.innerHTML = emailList;
};

const renderBookmarkPage = () => {
  const bookmarkIDs = JSON.parse(localStorage.getItem(bookmarkKey));
  const emailsElement = document.querySelector("#contacts-content");
  let emailList = ``;

  data
    .filter(val => bookmarkIDs.includes(val.id))
    .forEach(value => (emailList += getContactCardElement(value)));
  emailsElement.innerHTML = emailList;
};

document.addEventListener("DOMContentLoaded", function() {
  const elems = document.querySelectorAll(".sidenav");
  M.Sidenav.init(elems);
  loadNav();

  function loadNav() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status != 200) return;

        document.querySelectorAll(".topnav, .sidenav").forEach(function(elm) {
          elm.innerHTML = xhttp.responseText;
        });

        document
          .querySelectorAll(".sidenav a, .topnav a")
          .forEach(function(elm) {
            elm.addEventListener("click", function(event) {
              const sidenav = document.querySelector(".sidenav");
              M.Sidenav.getInstance(sidenav).close();

              page = event.target.getAttribute("href").substr(1);
              loadPage(page);
            });
          });
      }
    };
    xhttp.open("GET", "nav.html", true);
    xhttp.send();
  }

  loadPage(getPage());

  function loadPage(page) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        const content = document.querySelector(".body-content");
        if (this.status == 200) {
          content.innerHTML = xhttp.responseText;
          if (page === "contacts") {
            renderContentContactPage();
          } else if (page === "bookmarks") {
            renderBookmarkPage();
          }
        } else if (this.status == 404) {
          content.innerHTML = "<p>Halaman tidak ditemukan.</p>";
        } else {
          content.innerHTML = "<p>Ups.. halaman tidak dapat diakses.</p>";
        }
      }
    };
    xhttp.open("GET", "pages/" + page + ".html", true);
    xhttp.send();
  }
});
